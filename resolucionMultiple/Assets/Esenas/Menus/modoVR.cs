﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
public class modoVR : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(cargardispositivo("cardboard"));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator cargardispositivo(string dispositivo)
    {
        VRSettings.LoadDeviceByName(dispositivo);
        yield return null;
        VRSettings.enabled = true;
    }
}
