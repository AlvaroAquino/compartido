﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class modoVRwalk : MonoBehaviour {

    public Transform vrCamera;
    public float toggleAngle = 30.0f;
    public float speed = 3.0f;
    public bool moveForward;
    public bool moveLeft;
    public bool moveRight;
    public bool jump;
    double aux=-1;
    public double SizeJump; 
    private CharacterController cc;

    // Use this for initialization
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {   // movimiento hacia adelante
        if (vrCamera.eulerAngles.x >= toggleAngle && vrCamera.eulerAngles.x < 90.0f) moveForward = true;
        else moveForward = false;

        if (moveForward)
        {
            Vector3 forward = vrCamera.TransformDirection(Vector3.forward);
            cc.SimpleMove(forward * speed);
        }

        // movimiento a la derecha
        if (vrCamera.eulerAngles.z >= toggleAngle && vrCamera.eulerAngles.z < 90.0f) moveRight = true;
        else moveRight = false;
        if (moveRight)
        {
            Vector3 derecha = vrCamera.TransformDirection(Vector3.right);
            cc.SimpleMove(derecha * speed);
        }

        // movimiento a la izquierda
        if (vrCamera.eulerAngles.z <= 360 - toggleAngle && vrCamera.eulerAngles.z > 270.0f) moveLeft = true;
        else moveLeft = false;
        if (moveLeft)
        {
            Vector3 izquierda = vrCamera.TransformDirection(Vector3.left);
            cc.SimpleMove(izquierda * speed);
        }

        //movimiento para saltar
        if (Input.GetKey(KeyCode.T))
        {
            jump = true;
            if (aux == -1)
            {
                aux = transform.position.y;
            }

        }
        if (jump)
        {
            print(transform.position.y-aux);
            if (transform.position.y - aux < SizeJump )
            {
                transform.Translate(0,0.1f, 0);
            }
            else
            {
                jump = false;
            }
        }
        else
        {
            if (transform.position.y > aux && aux!=-1 )
            {
                transform.Translate(0, -0.1f, 0);
            }
            else aux = -1;
        }
    }
}
