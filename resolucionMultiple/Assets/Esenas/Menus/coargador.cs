﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.SceneManagement;

public class coargador : MonoBehaviour {
    public bool espera;
	// Use this for initialization
	void Start () {
        if (espera == true) StartCoroutine(tiempoGarga());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void cargarEsena(string esena)
    {
        SceneManager.LoadScene(esena,LoadSceneMode.Single);
    }
    public void finalizar(string esena)
    {
        SceneManager.UnloadSceneAsync(esena);
    }
    IEnumerator tiempoGarga()
    {
      
        yield return new WaitForSeconds(10);
        SceneManager.LoadScene("esenario", LoadSceneMode.Single);
        //StartCoroutine(cargardispositivo("cardboard"));
    }
    IEnumerator cargardispositivo(string dispositivo)
    {
        VRSettings.LoadDeviceByName(dispositivo);
        yield return null;
        VRSettings.enabled = true;
    }
}
